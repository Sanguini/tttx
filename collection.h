#ifndef COLLECTION_H_INCLUDED
#define COLLECTION_H_INCLUDED
#include <iostream>
#include <fstream>
#include <windows.h>
#include <string>
#include <stdlib.h>
#include <conio.h>
#include <thread>
#include <time.h>

using namespace std;


bool contains(string str, char mark) {
	bool ret = false;
	for (unsigned int i = 0; i < str.length(); i++) {
		if (str[i] == mark)
			ret = true;
	}

	return ret;
}


bool checkHor(string* str, char mark, int stringListLength, int length, int rowLength) {
	bool success = false;
	int tempRun = 0;
	int origRowLength = rowLength;
	int colMul = 1; //Actually that would be easier handling this like a real table not just pseudoing it

	for (int i = 0; i < stringListLength; i++) {
		if (rowLength - length < i) {
			if (tempRun > length)	//Might fail :/
			{
				tempRun = 0;
				colMul++;
				rowLength = origRowLength * colMul;
			}
			else {
				tempRun++;
			}
		}
		else {
			if (contains(str[i], mark)) {
				for (int x = 0; x < length; x++) {
					if (contains(str[i + x], mark)) {
						success = true;
					}
					else {
						success = false;
						break;
					}
				}
				if (success)
					break;
			}
		}
	}

	return success;
}

bool checkVer1(string* str, char mark, int stringListLength, int length, int rowLength) {

	bool success = false;
	int origRowLength = rowLength;
	int colMul = 1;

	for (int i = 0; i < stringListLength; i++) {
		if( i > rowLength - length && rowLength > i) {
			continue;
		}else if (i > (stringListLength / origRowLength - length) * origRowLength + origRowLength) {
			if (i == rowLength) {
				colMul++;
				rowLength = colMul * origRowLength;
			}
				continue;
		}
		else {
			if (i == rowLength) {
				colMul++;
				rowLength = colMul * origRowLength;
			}
			if (contains(str[i], mark)) {
				for (int x = 0; x < length; x++) {
					if (contains(str[i + x + (x*origRowLength)],mark)) {
						success = true;
					}
					else {
						success = false;
						break;
					}
				}
				if (success)
					break;
			}
		}
	}
	return success;
}

bool checkVer2(string* str, char mark, int stringListLength, int length, int rowLength) {
	bool success = false;
	int fuckuRowLength = rowLength;
	int rowMulti = 1;
	for (int i = 0; i < stringListLength; i++) {
		if (i < fuckuRowLength + length && i > fuckuRowLength) {
			if (i == fuckuRowLength + length)
			{
				rowMulti++;
				fuckuRowLength = rowMulti * rowLength;
			}
		}
		else if (i > (stringListLength / rowLength - length) * rowLength + rowLength) {
			continue;
		}
		else {
			if (contains(str[i], mark))
			{
				for (int x = 0; x < length; x++) {
					if (contains(str[i - x + (x*rowLength)], mark)) {
						success = true;
					}
					else {
						success = false;
						break;
					}
				}
				if (success)
					break;
			}
		}
	}
	return success;

}

bool checkVer(string* str, char mark, int stringListLength, int length, int rowLength) {
	bool success = false;
	for (int i = 0; i < stringListLength; i++) {
		if (i > (stringListLength / rowLength - length) * rowLength + rowLength) {
			continue;
		}
		else {
			if (contains(str[i], mark)) {
				for (int x = 0; x < length; x++) {
					if (contains(str[i + (x * rowLength)], mark)) {
						success = true;
					}
					else {
						success = false;
						break;
					}
				}
				if (success)
					break;
			}
		}
	}
	return success;
}



int toNumber(string str)
{
	int result=0;
	for(int i=0; str.length(); i++)
	{
		if(str[i] == '0')
		{
			result= result * 10;
		}
		else if(str[i] == '1')
		{
			result = result * 10 + 1;
		}
		else if(str[i] == '2')
		{
			result = result * 10 + 2;
		}
		else if(str[i] == '3')
		{
			result = result * 10 + 3;
		}
		else if(str[i] == '4')
		{
			result = result * 10 + 4;
		}
		else if(str[i] == '5')
		{
			result = result * 10 + 5;
		}
		else if(str[i] == '6')
		{
			result = result * 10 + 6;
		}
		else if(str[i] == '7')
		{
			result = result * 10 + 7;
		}
		else if(str[i] == '8')
		{
			result = result * 10 + 8;
		}
		else if(str[i] == '9')
		{
			result = result * 10 + 9;
		}
	}
	return result;
}

string reverseString(string str)
{
	string sReversed;
	int iInd = 0;
	for(int i = str.length(); i != 0; i-- )
	{
		sReversed[iInd] = str[i];
		iInd++;
	}
	return sReversed;
}

inline HANDLE SetColor(HANDLE console,int fg=FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY, int bg=0)
{
	console=GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(console,fg | bg);
	return console;
}

inline void gotoxy(int x, int y)
{
	COORD ord;
	ord.X = x;
	ord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), ord);
}

inline void SlowPrint(char* string, int sleeptime)
{
	int i = 0;
	while(string[i] != '\0')
	{
		cout<<string[i];
		Sleep(sleeptime);
		i++;
	}
}

BOOL FileExists(const std::string & StrFilename)
{
	return( (GetFileAttributes(StrFilename.c_str()) == INVALID_FILE_ATTRIBUTES)
			? FALSE  //lesen nicht m�glich -> Datei existiert nicht!
			: TRUE); //die Datei existiert
}
#endif
