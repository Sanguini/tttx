#define  _WIN32_WINNT 0x0500
#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
#include <windows.h>
#include <fstream>
#include "OptionCnt.h"
#include "player.h"
#include "collection.h"
#include "menus.h"

//TIC TAC TODESFALLE XPANDED
/*

	10 x 10 field 5? in the row

*/

// Get a player manager in there where you can register and or unregister users so you only give the user id and the manager will give everything else out
//Optioncontainer
//COSTUMIZATION FOR EACH PLAYER

using namespace std;

string CAMPA = "Hello and Welcome to Tic Tac Todesbrawl (XPanded Edition)(Full Edition)(GOTY Edition)";


bool exitFlag = false;

bool isLoaded = false;

bool bFlag = false; //Cheat Win

void initFields();      //initialize all vars
void load();            //Load a saved game
void save();            //Save a game
void drawUi();          //Draws the field
bool checkForWin();   //Checks for 5 in the row //Maybe do it with int inrow
void drawPlayer();     //Draws UI
bool drawWin();
bool isInMask(char mark);

int main(int argc, char **argv)
{
    do
    {
        //Add Menu here
        while(!exitFlag)
            switch (mainMenu())
            {
            case start:
                exitFlag = true;
                break;
            case config:
                configMenu();
                break;
            case col:
                colMen();
                break;
            case loadG:
                load();
                exitFlag = true;
                isLoaded = true;
                break;
            case multi:

                break;
            default:

                break;
            }

        if(!isLoaded){
            OptionManager.init();
            initFields();

            //Costumize Players

            cout << "Costumization..." << endl;
            if (OptionManager.getShouldCostum())
            {
                system("cls");
                for (int i = 0; i < OptionManager.getPlayerCount(); i++)
                {
                    string answer;
                    system("cls");
                    cout << "Player " << i << " Name: ";
                    cin >> answer;
                    OptionManager.getPlayer(i)->setName(answer);
                    while(true)
                    {
                        cout << "Enter your Mark: " << endl;
                        cin >> answer;
                        if(isInMask(answer[0]))
                        {
                            OptionManager.getPlayer(i)->setMark(answer[0]);
                            break;
                        }else{
                            cout << "Your mark shall not be a number!" << endl;
                        }
                    }
                }
            }
            else
            {
                cout << "Default..." << endl;
                if(OptionManager.getPlayerCount() > 0){
                cout << "Player 1 init" << endl;
                OptionManager.getPlayer(0)->setName("Default 1");
                OptionManager.getPlayer(0)->setMark('O');
                }
                if(OptionManager.getPlayerCount() > 1){
                cout << "Player 2 init" << endl;
                OptionManager.getPlayer(1)->setName("Default 2");
                OptionManager.getPlayer(1)->setMark('X');
                }
                if(OptionManager.getPlayerCount() > 2){
                cout << "Player 3 init" << endl;
                OptionManager.getPlayer(2)->setName("Default 3");
                OptionManager.getPlayer(2)->setMark('P');
                }
                if(OptionManager.getPlayerCount() > 3){
                cout << "Player 4 init" << endl;
                OptionManager.getPlayer(3)->setName("Default 4");
                OptionManager.getPlayer(3)->setMark('B');
                }
                if(OptionManager.getPlayerCount() > 4){
                cout << "Player 5 init" << endl;
                OptionManager.getPlayer(4)->setName("Default 5");
                OptionManager.getPlayer(4)->setMark('Z');
                }
            }
            cout << "Costumized" << endl;
        }

        while(!OptionManager.someOneHasWon())
        {
            drawUi();
            drawPlayer();
            checkForWin();
            OptionManager.nextPlayer();
        }
    }
    while (drawWin());

    return 0;
}


bool isInMask(char mark)
{
    if(mark == '0')
        return false;
    if(mark == '1')
        return false;
    if(mark == '2')
        return false;
    if(mark == '3')
        return false;
    if(mark == '4')
        return false;
    if(mark == '5')
        return false;
    if(mark == '6')
        return false;
    if(mark == '7')
        return false;
    if(mark == '8')
        return false;
    if(mark == '9')
        return false;
    if(mark == ' ')
        return false;

    for(int i = 0; i < OptionManager.getPlayerCount(); i++)
    {
        if(OptionManager.getPlayer(i)->getMark() == mark)
            return false;
    }

    return true;
}

bool drawWin()
{
    //GET ALL PLAYERS
    //ITERATE THROU PLAYERS
    //IF HAS WON WRITE IT
    system("cls");
    for(int i = 0; i < OptionManager.getPlayerCount(); i++)
    {
        if(OptionManager.getPlayer(i)->getWon())
        {
            cout << "Congratz " << OptionManager.getPlayer(i)->getName() << " you won!" << endl;
            cout << "Wanna try again?" << endl << "::> ";
            break;
        }
    }
    string answer;
    getline(cin,answer);
    if(contains(answer,'y'))
        return true;
    return false;
}


bool to_bool(std::string const& s)
{
    return s != "0";
}


void load()
{
    	ifstream in;
    	in.open("Save.dat");
    	if(!in.good())
    		return;
    	string temp;
        getline(in,temp);
        OptionManager.setColLength(atoi(temp.c_str()));
        getline(in, temp);
        OptionManager.setPadding(atoi(temp.c_str()) + 1);
        getline(in, temp);
        OptionManager.setPlayerCount(atoi(temp.c_str()));
        getline(in,temp);
        OptionManager.setRowLength(atoi(temp.c_str()));
        getline(in, temp);
        OptionManager.setWinLength(atoi(temp.c_str()));

        OptionManager.init();

        for(int i = 0; i < OptionManager.getPlayerCount(); i++){
            getline(in, temp);
            OptionManager.getPlayer(i)->setMark(temp[0]);
            getline(in, temp);
            OptionManager.getPlayer(i)->setName(temp);
        }

        for(int i = 0; i < OptionManager.getFieldsNumber(); i++)
        {
            getline(in, temp);
            OptionManager.getStringList()[i] = temp;
        }
}

void save()
{
    	ofstream of;
    	of.open("Save.dat");
    	if(!of.good())
    		return;

        of << OptionManager.getColLength() << endl;
        of << OptionManager.getPadding().length() << endl;
        of << OptionManager.getPlayerCount() << endl;
        of << OptionManager.getRowLength() << endl;
        of << OptionManager.getWinLength() << endl;

        //Player
        for(int i = 0; i < OptionManager.getPlayerCount();i++)
        {
            of << OptionManager.getPlayer(i)->getMark() << endl;
            of << OptionManager.getPlayer(i)->getName() << endl;
        }

        //Stringlist
        for(int i = 0; i < OptionManager.getFieldsNumber();i++)
        {
            of << OptionManager.getStringList()[i] << endl;
        }
}

bool isInField(string str, int* cunt)
{
    bool success = false;
    for(int i = 0; i < OptionManager.getFieldsNumber(); i++)
    {
        if((OptionManager.getStringList())[i] == str)
        {
            success = true;
            *cunt = i;
            break;
        }
    }
    return success;
}

void drawPlayer()
{
    bool emptyFlag = false;
    while(true)
    {
        string answer;
        int nr; //BE SURE TO INIT DIS!
        nr = 1337;
        if(!emptyFlag)
            cout << OptionManager.getActivePlayer()->getName() << " is setting: " << OptionManager.getActivePlayer()->getMark() << endl << "::> ";
        else
            emptyFlag = false;
        getline(cin,answer);
        if(answer == "")
        {
            emptyFlag = true;
            continue;
        }
        else if (answer == "bananenpudding")		//MENUHOOK HERE!
            bFlag = true;
        else if(answer == "save")
        {
            save();
            CAMPA = "Saved the Game!";
            drawUi();
        }
        else if(answer == "load")
        {
            load();
            CAMPA = "Loaded the Game!";
            drawUi();
        }else if(answer == "exit")
        {
            exit(0);
        }else if(answer == "help")
        {
            CAMPA = "save, load, exit";
            drawUi();
        }
        else if(isInField(answer,&nr))
        {
            if(nr != 1337)
            {
                string temp = OptionManager.getPadding() + OptionManager.getActivePlayer()->getMark();
                OptionManager.getStringList()[nr] = temp;
            }
            CAMPA = OptionManager.getActivePlayer()->getName() + " has set: " + answer;
            break;
        }
        else
        {
            CAMPA = answer + " isnt valid";
            drawUi();
        }
    }

}

bool checkForWin()//,int long)
{
    if(checkHor(OptionManager.getStringList(), OptionManager.getActivePlayer()->getMark(), OptionManager.getFieldsNumber(), OptionManager.getWinLength(), OptionManager.getRowLength()))
    {
        OptionManager.getActivePlayer()->setWon(true);
        return true;
    }
    else if(checkVer(OptionManager.getStringList(), OptionManager.getActivePlayer()->getMark(), OptionManager.getFieldsNumber(), OptionManager.getWinLength(), OptionManager.getRowLength()))
    {
        OptionManager.getActivePlayer()->setWon(true);
        return true;
    }
    else if(checkVer1(OptionManager.getStringList(), OptionManager.getActivePlayer()->getMark(), OptionManager.getFieldsNumber(), OptionManager.getWinLength(), OptionManager.getRowLength()))
    {
        OptionManager.getActivePlayer()->setWon(true);
        return true;
    }
    else if(checkVer2(OptionManager.getStringList(), OptionManager.getActivePlayer()->getMark(), OptionManager.getFieldsNumber(), OptionManager.getWinLength(), OptionManager.getRowLength()))
    {
        OptionManager.getActivePlayer()->setWon(true);
        return true;
    }

    if (bFlag)
    {
        OptionManager.getActivePlayer()->setWon(true);
        return true;
    }

    return false;
}

string intToString(int i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

void drawUi()
{
    system("cls");
    cout << CAMPA << endl << endl;
    for(int i = 0; i < OptionManager.getFieldsNumber(); i++)
    {
        cout << OptionManager.getStringList()[i];
        if((i + 1) % OptionManager.getRowLength() == 0 && i != 0)
            cout << endl;
        else
            cout << " | ";
    }
    cout << endl << endl;

}

string getPadding(int x, int iMax)
{
    string padding = "";
    int temp = (iMax - x);
    for(int i = 0; i < temp; i++)
    {
        padding += '0';
    }

    return padding;
}

void initFields()
{
    system("title Tic Tac Todesbrawl");
    OptionManager.setPadding(intToString(OptionManager.getFieldsNumber() - 1).length() - 1);
    int paddingMax = intToString(OptionManager.getFieldsNumber() - 1).length() - 1;
    cout << "Init Fields" << endl;
    for(int i = 0; i < OptionManager.getFieldsNumber(); i++)
    {
        OptionManager.getStringList()[i] = getPadding(intToString(i).length() - 1, paddingMax) + intToString(i);
    }
    cout << "intialised" << endl;
}

