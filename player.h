#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
#include <iostream>
#include <string>

struct player{
private:
    bool hasWon = false;
    char mark = 'X';
    std::string name = "Uninitialized";
public:
    player(std::string name, char mark);
    bool getWon();
    void setWon(bool winState);
    char getMark();
    void setMark(char mark);
    std::string getName();
    void setName(std::string name);
};

player::player(std::string name,char mark){
    player::mark = mark;
    player::name = name;
}

bool player::getWon(){
    return player::hasWon;
}

void player::setWon(bool winState){
    player::hasWon = winState;
}

char player::getMark(){
    return player::mark;
}

void player::setMark(char mark){
    player::mark = mark;
}

void player::setName(std::string name){
    player::name = name;
}

std::string player::getName(){
    return player::name;
}

#endif // PLAYER_H_INCLUDED
