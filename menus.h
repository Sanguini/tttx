#ifndef MENUS_H_INCLUDED
#define MENUS_H_INCLUDED


/*
	MAYBE MAKE MENU CLASSES
	WOULD BE EASIER TO DRAW
*/

enum colMenus
{
    redf,
    bluef,
    intensf,
    greenf,
    redb,
    blueb,
    intensb,
    greenb,
    acid,
    rainbow,
    colBack,
    totalColMen
};


void colMen()
{
    int selected = 0;
    bool shouldTrigger = false;
    bool shouldDraw = true;
    int iKeyOnePressTMR = clock();
    int foreGroundCol = 0;
    int backGroundCol = 0;

    while(true)
    {
        if(shouldDraw)
        {
            system("cls");
            shouldDraw = false;
            switch(selected)
            {
            case redf:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "--> " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case bluef:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "--> " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case intensf:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "--> " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case greenf:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "--> " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case redb:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "--> " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case blueb:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "--> " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case intensb:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "--> " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case greenb:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "--> " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case acid:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "--> " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case rainbow:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "--> " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "    " << "Back" << endl;
                break;

            case colBack:
                cout << "  FOREGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "  BACKGROUND COLOUR" << endl;
                cout << "    " << "RED" << endl;
                cout << "    " << "BLUE" << endl;
                cout << "    " << "INTENSITY" <<endl;
                cout << "    " << "GREEN" << endl << endl;
                cout << "    " << "Wanna go on an Acid Trip?" << endl;
                cout << "    " << "DOUBLE RAINBOW!!!" << endl << endl;
                cout << "--> " << "Back" << endl;
                break;

            default:
                selected = 0;
                break;
            }
        }

        if(shouldTrigger)
        {
            shouldTrigger = false;
            switch(selected)
            {
            case redf:
                foreGroundCol = FOREGROUND_RED;
                SetColor(NULL,FOREGROUND_RED,backGroundCol);
                break;
            case bluef:
                foreGroundCol = FOREGROUND_BLUE;
                SetColor(NULL, FOREGROUND_BLUE,backGroundCol);
                break;
            case intensf:
                foreGroundCol = FOREGROUND_INTENSITY;
                SetColor(NULL,FOREGROUND_INTENSITY,backGroundCol);
                break;
            case greenf:
                foreGroundCol = FOREGROUND_GREEN;
                SetColor(NULL, FOREGROUND_GREEN,backGroundCol);
                break;
            case redb:
                backGroundCol = BACKGROUND_RED;
                SetColor(NULL, foreGroundCol, BACKGROUND_RED);
                break;
            case blueb:
                backGroundCol = BACKGROUND_BLUE;
                SetColor(NULL,foreGroundCol,BACKGROUND_BLUE);
                break;
            case intensb:
                backGroundCol = BACKGROUND_INTENSITY;
                SetColor(NULL,foreGroundCol,BACKGROUND_INTENSITY);
                break;
            case greenb:
                backGroundCol = BACKGROUND_GREEN;
                SetColor(NULL,foreGroundCol,BACKGROUND_GREEN);
                break;
            case acid:
                OptionManager.enableAcidThread();
                break;
            case rainbow:
                OptionManager.enableRainbowThread();
                break;
            case colBack:
                return;
                break;
            default:
                break;
            }

        }

        if (GetConsoleWindow() == GetForegroundWindow())
        {
            if(clock()-iKeyOnePressTMR > 300)
            {
                if (GetAsyncKeyState(VK_CONTROL))
                {
                    shouldTrigger = true;
                    iKeyOnePressTMR = clock();
                }
                if (GetAsyncKeyState(VK_DOWN) || GetAsyncKeyState(0x53))
                {
                    selected++;
                    if (selected == totalColMen)
                    {
                        selected = 0;
                    }
                    shouldDraw = true;
                    iKeyOnePressTMR = clock();
                }

                if (GetAsyncKeyState(VK_UP) || GetAsyncKeyState(0x57))
                {
                    selected--;
                    if (selected < 0)
                    {
                        selected = totalColMen - 1;
                    }
                    shouldDraw = true;
                    iKeyOnePressTMR = clock();
                }
            }
        }
    }
}

enum cMenus
{
    playerCnt,
    winLength,
    fieldX,
    fieldY,
    standartisized,
    buyDlc,
    back,
    totalCMen
};

bool configMenu()    //WE NEED ARGS!
{

    int selected = 0;
    bool shouldTrigger = false;
    bool shouldDraw = true;
    int iKeyOnePressTMR = clock();

    while(true)
    {
        if(shouldDraw)
        {
            system("cls");
            shouldDraw = false;
            switch (selected)
            {
            case playerCnt:
                cout << " --> " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case winLength:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << " --> " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case fieldX:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << " --> " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case fieldY:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << " --> " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case standartisized:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << " --> " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case buyDlc:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << " --> " << "Buy DLCs: " << "$69,99" << endl;
                cout << "     " << "Back" << endl;
                break;
            case back:
                cout << "     " << "Change Player Count: " << OptionManager.getPlayerCount() << endl;
                cout << "     " << "Length of sames in a Row: " << OptionManager.getWinLength() << endl;
                cout << "     " << "Change Field width: " << OptionManager.getRowLength() << endl;
                cout << "     " << "Change Field breadth: " << OptionManager.getColLength() << endl;
                cout << "     " << "Should Customize of Player: " << OptionManager.getShouldCostum() << endl;
                cout << "     " << "Buy DLCs: " << "$69,99" << endl;
                cout << " --> " << "Back" << endl;
                break;
            default:
                selected = 0;
                break;
            }
        }

        if (shouldTrigger)
        {
            shouldTrigger = false;
            string ariba;
            switch (selected)
            {
            case playerCnt:
                cout << "Enter the Playercount: ";
                cin >> ariba;
                if(atoi(ariba.c_str()) <= 0)
                    cout << "Shall be greater than 0!" << endl;
                else if((atoi(ariba.c_str()) < 6 && !OptionManager.getShouldCostum()) || OptionManager.getShouldCostum())
                    OptionManager.setPlayerCount(atoi(ariba.c_str()));
                else
                {
                    cout << "Enable Costumization to Change this to something upper 5" << endl;
                }
                break;
            case winLength:
                cout << "Enter the number of characters in a row to win: ";
                cin >> ariba;
                if(atoi(ariba.c_str()) <= 0)
                    cout << "Shall be greater than 0!" << endl;
                else
                    OptionManager.setWinLength(atoi(ariba.c_str()));
                break;
            case fieldY:
                cout << "Enter the Field Breadth: ";
                cin >> ariba;
                if(atoi(ariba.c_str()) <= 0)
                    cout << "Shall be greater than 0!" << endl;
                else
                    OptionManager.setColLength(atoi(ariba.c_str()));
                break;
            case fieldX:
                cout << "Enter the Field Width: ";
                cin >> ariba;
                if(atoi(ariba.c_str()) <= 0)
                    cout << "Shall be greater than 0!" << endl;
                else
                    OptionManager.setRowLength(atoi(ariba.c_str()));
                break;
            case standartisized:
                if(OptionManager.getPlayerCount() > 5 && OptionManager.getShouldCostum())
                    cout << "Change the Player Count to something 5 or lower to disable this" << endl;
                else
                    OptionManager.setShouldCostum(!OptionManager.getShouldCostum());
                break;
            case buyDlc:
                cout << "You really wanna buy it?" << endl;
                cin >> ariba;
                if(contains(ariba,'y'))
                {
                    float accountBalance = -10;
                    if(!OptionManager.getDlcUnlocked() && accountBalance > 10000)
                    {
                        //Set DLC Unlocker true;
                        OptionManager.setDlcUnlocked(true);
                        cout << "Congratz you have dem Dlc�s for dis session" << endl;
                    }
                    else
                    {
                        cout << "You dont have dem Money!" << endl;
                        OptionManager.setDlcUnlocked(false);
                    }
                }
                else
                {
                    cout << "You are probably even not MLG enough to buy it" << endl;
                }
                break;
            case back:
                return false;
                break;
            default:
                break;
            }
        }

        if (GetConsoleWindow() == GetForegroundWindow())
        {
            if (clock() - iKeyOnePressTMR > 300)
            {
                if (GetAsyncKeyState(VK_CONTROL))
                {
                    shouldTrigger = true;
                    iKeyOnePressTMR = clock();
                }
                if (GetAsyncKeyState(VK_DOWN) || GetAsyncKeyState(0x53))
                {
                    selected++;
                    if (selected == totalCMen)
                    {
                        selected = 0;
                    }
                    shouldDraw = true;
                    iKeyOnePressTMR = clock();
                }

                if (GetAsyncKeyState(VK_UP) || GetAsyncKeyState(0x57))
                {
                    selected--;
                    if (selected < 0)
                    {
                        selected = totalCMen - 1;
                    }
                    shouldDraw = true;
                    iKeyOnePressTMR = clock();
                }
            }
        }
    }

    return false;
}

enum returnModes
{
    start,
    config,
    col,
    loadG,
    multi,
    totalrtn
};

enum menus
{
    StartGame,
    ConfigGame,
    ColorMenu,
    LoadGame,
    Multiplayer,
    ExitGame,
    totalMenus
};


int mainMenu()
{
    int selected = 0;
    bool shouldTrigger = false;
    bool shouldCls = true;
    int keyPressTmr = clock();
    bool upFlag = false;
    bool downFlag = true;

    system("title TTTX");

    while (true)
    {
        //Drawing
        if(shouldCls)
        {
            shouldCls = false;
            system("cls");
            cout << endl << endl << "   " << "###############################" << endl << "   #                             #" << endl << "   # Skipp3rs Tic Tac Todesbrawl #"<< endl << "   #                             #" << endl;
            switch (selected)
            {
            case StartGame:
                cout << "   # " << "--> " << "Start the Game!" << " <--" << "     #" << endl;
                cout << "   # " << "    " << "Configure the Game!" << "    " << " #" << endl;
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Color Menu!" << "         " << "    #" << endl;
                }
                cout << "   # " << "    " << "Load the last Game!" << "    " << " #" << endl;
                cout << "   # " << "    " << "Multiplayer Game!" << "    " << "   #" << endl;
                cout << "   # " << "    " << "Exit Game!" << "    " << "          #" << endl;
                break;
            case ConfigGame:
                cout << "   # " << "    " << "Start the Game!" << "    " << "     #" << endl;
                cout << "   # " << "--> " << "Configure the Game!" << " <--" << " #" << endl;
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Color Menu!" << "         " << "    #" << endl;
                }
                cout << "   # " << "    " << "Load the last Game!" << "    " << " #" << endl;
                cout << "   # " << "    " << "Multiplayer Game!" << "    " << "   #" << endl;
                cout << "   # " << "    " << "Exit Game!" << "    " << "          #" << endl;
                break;
            case ColorMenu:
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Start the Game!" << "    " << "     #" << endl;
                    cout << "   # " << "    " << "Configure the Game!" << "    " << " #" << endl;
                    cout << "   # " << "--> " << "Color Menu!" << " <--  " << "       #" << endl;
                    cout << "   # " << "    " << "Load the last Game!" << "    " << " #" << endl;
                    cout << "   # " << "    " << "Multiplayer Game!" << "    " << "   #" << endl;
                    cout << "   # " << "    " << "Exit Game!" << "    " << "          #" << endl;
                }
                else
                {
                    if(upFlag)
                        selected = ColorMenu-1;
                    else if(downFlag)
                        selected = ColorMenu+1;
                    shouldCls = true;
                }
                break;
            case LoadGame:
                cout << "   # " << "    " << "Start the Game!" << "    " << "     #" << endl;
                cout << "   # " << "    " << "Configure the Game!" << "    " << " #" << endl;
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Color Menu!" << "         " << "    #" << endl;
                }
                cout << "   # " << "--> " << "Load the last Game!" << " <--" << " #" << endl;
                cout << "   # " << "    " << "Multiplayer Game!" << "    " << "   #" << endl;
                cout << "   # " << "    " << "Exit Game!" << "    " << "          #" << endl;
                break;
            case Multiplayer:
                cout << "   # " << "    " << "Start the Game!" << "    " << "     #" << endl;
                cout << "   # " << "    " << "Configure the Game!" << "    " << " #" << endl;
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Color Menu!" << "         " << "    #" << endl;
                }
                cout << "   # " << "    " << "Load the last Game!" << "    " << " #" << endl;
                cout << "   # " << "--> " << "Multiplayer Game!" << " <--" << "   #" << endl;
                cout << "   # " << "    " << "Exit Game!" << "    " << "          #" << endl;
                break;
            case ExitGame:
                cout << "   # " << "    " << "Start the Game!" << "    " << "     #" << endl;
                cout << "   # " << "    " << "Configure the Game!" << "    " << " #" << endl;
                if(OptionManager.getDlcUnlocked())
                {
                    cout << "   # " << "    " << "Color Menu!" << "         " << "    #" << endl;
                }
                cout << "   # " << "    " << "Load the last Game!" << "    " << " #" << endl;
                cout << "   # " << "    " << "Multiplayer Game!" << "     " << "  #" << endl;
                cout << "   # " << "--> " << "Exit Game!" << " <--" << "          #" << endl;
                break;
            default:
                selected = StartGame;
                break;
            }

            cout << "   #                             #" << endl;
            cout << "   ###############################" << endl;
        }


        if(shouldTrigger)
        {
            shouldTrigger = false;

            switch (selected)
            {
            case StartGame:
                return start;
                break;
            case ConfigGame:
                return config;
                break;
            case ColorMenu:
                if(OptionManager.getDlcUnlocked())
                    return ColorMenu;
                break;
            case LoadGame:
                //Jup definitively we need a global player Manager
                return loadG;
                break;
            case Multiplayer:
                cout << "Well i know that Networking is fucking crap so i have to get the hang of it" << endl;
                //return multi; TODO
                break;
            case ExitGame:
                exit(0);
                break;
            default:
                break;
            }

        }

        //Key handling

        if (GetConsoleWindow() == GetForegroundWindow())
        {
            if(clock() - keyPressTmr > 300)
            {
                if (GetAsyncKeyState(VK_CONTROL))
                {
                    shouldTrigger = true;
                    keyPressTmr = clock();
                }
                if (GetAsyncKeyState(VK_DOWN) || GetAsyncKeyState(0x53))
                {
                    downFlag = true;
                    upFlag = false;
                    selected++;
                    if (selected == totalMenus)
                    {
                        selected = 0;
                    }
                    shouldCls = true;
                    keyPressTmr = clock();
                }

                if (GetAsyncKeyState(VK_UP) || GetAsyncKeyState(0x57))
                {
                    upFlag = true;
                    downFlag = false;
                    selected--;
                    if (selected < 0)
                    {
                        selected = totalMenus - 1;
                    }
                    shouldCls = true;
                    keyPressTmr = clock();
                }
            }
        }
    }
}



#endif // MENUS_H_INCLUDED
