#ifndef OPTIONCNT_H_INCLUDED
#define OPTIONCNT_H_INCLUDED
#include "collection.h"
#include "player.h"
#include <iostream>

using namespace std;

struct OptionContainer
{

private:
    int rowLength = 10;
    int colLength = 10;
    player* Player[100];
    char playerMarksArray[100] = {};
    int playerActiveCount = 0;
    int playerCount = 2;
    bool shouldCostumize = false;

    string padding = " ";

    string* StringList;

    bool dlcUnlocked = false;

    int iWinLength = 3;

    bool acidThreadCreated = false;
    bool rainbowThreadCreated = false;

public:
    void init();

    string getPadding();

    void setPadding(unsigned int i);

    void enableRainbowThread();
    void enableAcidThread();

    int getFieldsNumber();
    int getRowLength();
    int getColLength();
    void setRowLength(int i);
    void setColLength(int i);

    int getWinLength();
    void setWinLength(int i);

    player* getPlayer(int i);
    player** getPlayerArray();

    void setPlayerCount(int i);
    int getPlayerCount();

    string* getStringList()
    {
        return StringList;
    }

    bool getShouldCostum();
    void setShouldCostum(bool state);

    inline void setDlcUnlocked(bool state);
    inline bool getDlcUnlocked();

    player* getActivePlayer();
    void nextPlayer();

    bool someOneHasWon();

    ~OptionContainer() {}

} OptionManager;


string OptionContainer::getPadding()
{
    return OptionContainer::padding;
}

void OptionContainer::setPadding(unsigned int i)
{
    OptionContainer::padding = "";

    for(unsigned int x = 0; x < i; x++)
    {
        OptionContainer::padding += " ";
    }
}


void RainbowTripThread()
{
    while(true)
    {
        for(int i = 0; i < 9; i++)
            for(int x = 0; x < 9; x++)
                SetColor(NULL,i,x);
    }
}

void OptionContainer::enableRainbowThread()
{
    if(!OptionContainer::acidThreadCreated && !OptionContainer::rainbowThreadCreated){
        OptionContainer::rainbowThreadCreated = true;
        DWORD Temp;
        CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)RainbowTripThread,NULL,0,&Temp);
    }

}

void AcidTripThread()
{
    while(true)
    {
        system("color 01");
        system("color 12");
        system("color 23");
        system("color 34");
        system("color 45");
        system("color 56");
        system("color 67");
        system("color 78");
        system("color 89");
        system("color 9a");
        system("color ab");
        system("color bc");
        system("color cd");
        system("color de");
        system("color ef");
        system("color f0");
    }
}

void OptionContainer::enableAcidThread()
{
    if(!OptionContainer::acidThreadCreated && !OptionContainer::rainbowThreadCreated){
        OptionContainer::acidThreadCreated = true;
        DWORD Temp;
        CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)AcidTripThread,NULL,0,&Temp);
    }

}

int OptionContainer::getWinLength()
{
    return OptionContainer::iWinLength;
}

void OptionContainer::setWinLength(int i)
{
    OptionContainer::iWinLength = i;
}

bool OptionContainer::someOneHasWon()
{
    for(int i = 0; i < OptionContainer::playerCount; i++)
    {
        if(OptionContainer::Player[i]->getWon())
            return true;
    }
    return false;
}

void OptionContainer::init()
{
    OptionContainer::StringList = new string[OptionContainer::getFieldsNumber()];
    for( int i = 0; i < OptionContainer::playerCount ; i++)
    {
        OptionContainer::Player[i] = new player("Default", 'D');
    }
}

int OptionContainer::getFieldsNumber()
{
    return OptionContainer::rowLength * OptionContainer::colLength;
}

int OptionContainer::getColLength()
{
    return OptionContainer::colLength;
}

int OptionContainer::getRowLength()
{
    return OptionContainer::rowLength;
}

void OptionContainer::setRowLength(int i)
{
    OptionContainer::rowLength = i;
}
void OptionContainer::setColLength(int i)
{
    OptionContainer::colLength = i;
}

player* OptionContainer::getPlayer(int i)
{
    return OptionContainer::Player[i];
}
player** OptionContainer::getPlayerArray()
{
    return OptionContainer::Player;
}

void OptionContainer::setPlayerCount(int i)
{
    OptionContainer::playerCount = i;
}
int OptionContainer::getPlayerCount()
{
    return OptionContainer::playerCount;
}

bool OptionContainer::getShouldCostum()
{
    return OptionContainer::shouldCostumize;
}
void OptionContainer::setShouldCostum(bool state)
{
    OptionContainer::shouldCostumize = state;
}

inline void OptionContainer::setDlcUnlocked(bool state)
{
    OptionContainer::dlcUnlocked = state;
}

inline bool OptionContainer::getDlcUnlocked()
{
    return OptionContainer::dlcUnlocked;
}


player* OptionContainer::getActivePlayer()
{
    return OptionContainer::Player[OptionContainer::playerActiveCount];
}

void OptionContainer::nextPlayer()
{
    OptionContainer::playerActiveCount++;
    if(OptionContainer::playerCount <= OptionContainer::playerActiveCount)
    {
        OptionContainer::playerActiveCount = 0;
    }
}

#endif // OPTIONCNT_H_INCLUDED
